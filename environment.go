package configgen

import (
	"os"
	"strconv"
)

const TAG = "conf"

func setConfVar(key string, value interface{}) {
	newValue, found := os.LookupEnv(key)
	if !found {
		return
	}
	isString, err := tryString(value, newValue)
	if isString {
		return
	}
	isBool, err := tryBool(value, newValue)
	if isBool {
		if err != nil {
			panic("Unable to convert field to a bool: " + key)
		}
		return
	}
	isFloat, err := tryFloat(value, newValue)
	if isFloat {
		if err != nil {
			panic("Unable to convert field to a float: " + key)
		}
		return
	}
	isInt, err := tryInt(value, newValue)
	if isInt {
		if err != nil {
			panic("Unable to convert field to an int: " + key)
		}
		return
	}
}

func tryString(value interface{}, newValue string) (bool, error) {
	isType := false
	switch value.(type) {
	case *string:
		isType = true
		sptr := value.(*string)
		*sptr = newValue
	}
	return isType, nil
}

func tryBool(value interface{}, newValue string) (bool, error) {
	isType := false
	var err error
	var set bool
	switch value.(type) {
	case *bool:
		isType = true
		bptr := value.(*bool)
		set, err = strconv.ParseBool(newValue)
		if err == nil {
			*bptr = set
		}
	}
	return isType, err
}

func tryFloat(value interface{}, newValue string) (bool, error) {
	isType := false
	var err error
	var set float64
	switch value.(type) {
	case *float64:
		isType = true
		fptr := value.(*float64)
		set, err = strconv.ParseFloat(newValue, 64)
		if err == nil {
			*fptr = set
		}
	case *float32:
		isType = true
		fptr := value.(*float32)
		set, err = strconv.ParseFloat(newValue, 32)
		if err == nil {
			*fptr = float32(set)
		}
	}
	return isType, err
}

func tryInt(value interface{}, newValue string) (bool, error) {
	//Flag to terminate on success
	isType := false
	var superErr error
	//Int Types
	switch value.(type) {
	case *uint:
		isType = true
		set, err := strconv.ParseUint(newValue, 10, 64)
		superErr = err
		iptr := value.(*uint)
		if err == nil {
			*iptr = uint(set)
		}
	case *int:
		isType = true
		set, err := strconv.Atoi(newValue)
		superErr = err
		iptr := value.(*int)
		if err == nil {
			*iptr = set
		}
	case *uint64:
		isType = true
		set, err := strconv.ParseUint(newValue, 10, 64)
		superErr = err
		iptr := value.(*uint64)
		if err == nil {
			*iptr = set
		}
	case *int64:
		isType = true
		set, err := strconv.ParseInt(newValue, 10, 64)
		superErr = err
		iptr := value.(*int64)
		if err == nil {
			*iptr = set
		}
	case *uint32:
		isType = true
		set, err := strconv.ParseUint(newValue, 10, 32)
		superErr = err
		iptr := value.(*uint32)
		if err == nil {
			*iptr = uint32(set)
		}
	case *int32:
		isType = true
		set, err := strconv.ParseInt(newValue, 10, 32)
		superErr = err
		iptr := value.(*int32)
		if err == nil {
			*iptr = int32(set)
		}
	case *uint16:
		isType = true
		set, err := strconv.ParseUint(newValue, 10, 16)
		superErr = err
		iptr := value.(*uint16)
		if err == nil {
			*iptr = uint16(set)
		}
	case *int16:
		isType = true
		set, err := strconv.ParseInt(newValue, 10, 16)
		superErr = err
		iptr := value.(*int16)
		if err == nil {
			*iptr = int16(set)
		}
	case *uint8:
		isType = true
		set, err := strconv.ParseUint(newValue, 10, 8)
		superErr = err
		iptr := value.(*uint8)
		if err == nil {
			*iptr = uint8(set)
		}
	case *int8:
		isType = true
		set, err := strconv.ParseInt(newValue, 10, 8)
		superErr = err
		iptr := value.(*int8)
		if err == nil {
			*iptr = int8(set)
		}
	}
	return isType, superErr
}

func LoadEnvConfig(ConfStruct interface{}) {
	LoadEnvConfigWithPrefix(ConfStruct, "")
}

func LoadEnvConfigWithPrefix(ConfStruct interface{}, prefix string) {
	LoadEnvConfigWithTag(ConfStruct, prefix, TAG)
}

func LoadEnvConfigWithTag(ConfStruct interface{}, prefix string, tag string) {
	if ConfStruct == nil {
		panic("Can't load config into nil")
	}
	mapping := ParseKeys(ConfStruct, prefix, tag)
	for k, v := range mapping {
		setConfVar(k, v)
	}
}
