package configgen

type AllTypesConfig struct {
	String string

	Bool bool

	Int   int
	Int64 int64
	Int32 int32
	Int16 int16
	Int8  int8

	Uint   uint
	Uint64 uint64
	Uint32 uint32
	Uint16 uint16
	Uint8  uint8

	Float64 float64
	Float32 float32
}
