package configgen

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseKeysTags(t *testing.T) {
	defaultTagsConfig := struct {
		Name  string
		Count int
	}{
		"",
		0,
	}
	mapping := ParseKeys(&defaultTagsConfig, "", "conf")
	assert.NotNil(t, mapping["Name"])
	assert.NotNil(t, mapping["Count"])
	assert.Equal(t, len(mapping), 2)

	overrideTagsConfig := struct {
		Name  string `conf:"field1"`
		Count int    `conf:"field2"`
		Good  bool   `conf:"field3"`
	}{
		"",
		0,
		false,
	}
	mapping = ParseKeys(&overrideTagsConfig, "", "conf")
	assert.NotNil(t, mapping["field1"])
	assert.NotNil(t, mapping["field2"])
	assert.NotNil(t, mapping["field3"])
	assert.Equal(t, len(mapping), 3)
}

func TestParseKeysString(t *testing.T) {

	name := "George"
	config := struct {
		Name string `conf:"field1"`
	}{
		name,
	}

	mapping := ParseKeys(&config, "", "conf")

	assert.NotNil(t, mapping["field1"], "Expected Tag override in map")
	assert.Nil(t, mapping["Name"], "Default Tag is not defined")
	value, found := mapping["field1"].(*string)
	assert.True(t, found, "Value was not a string")
	assert.Equal(t, *value, name, "Expected Values to match")
	assert.Same(t, value, &config.Name, "Expected pointers to match")

	name = "Alice"
	defaultTagConfig := struct {
		Name string
	}{
		name,
	}

	mapping = ParseKeys(&defaultTagConfig, "", "conf")

	assert.NotNil(t, mapping["Name"], "Default tag value should be the same as the key")
	assert.Nil(t, mapping["field1"], "Override tag not present")
	value, found = mapping["Name"].(*string)
	assert.True(t, found, "Value was not a string")
	assert.Equal(t, *value, name, "Expected Values to match")
	assert.Same(t, value, &defaultTagConfig.Name, "Expected pointers to match")
}

func TestParseKeysInt(t *testing.T) {

	config := struct {
		I64 int64
		I32 int32
		I16 int16
		I8  int8
		I   int
	}{
		64,
		32,
		16,
		8,
		2,
	}
	mapping := ParseKeys(&config, "", "conf")
	//64 bit
	assert.NotNil(t, mapping["I64"])
	i64, found := mapping["I64"].(*int64)
	assert.True(t, found, "Expected Int64")
	assert.Equal(t, config.I64, *i64)
	// 32 bit
	assert.NotNil(t, mapping["I32"])
	i32, found := mapping["I32"].(*int32)
	assert.True(t, found, "Expected Int32")
	assert.Equal(t, config.I32, *i32)
	//16 bit
	assert.NotNil(t, mapping["I16"])
	i16, found := mapping["I16"].(*int16)
	assert.True(t, found, "Expected Int16")
	assert.Equal(t, config.I16, *i16)
	// 8 bit
	assert.NotNil(t, mapping["I8"])
	i8, found := mapping["I8"].(*int8)
	assert.True(t, found, "Expected Int8")
	assert.Equal(t, config.I8, *i8)
	// Default
	assert.NotNil(t, mapping["I"])
	i, found := mapping["I"].(*int)
	assert.True(t, found, "Expected Int")
	assert.Equal(t, config.I, *i)
}

func TestParseKeysBool(t *testing.T) {
	config := struct {
		T bool
		F bool
	}{
		true,
		false,
	}

	mapping := ParseKeys(&config, "", "conf")

	assert.NotNil(t, mapping["T"])
	value, found := mapping["T"].(*bool)
	assert.True(t, found, "Proper type: boolean")
	assert.Same(t, &config.T, mapping["T"])
	assert.Equal(t, true, *value)

	assert.NotNil(t, mapping["F"])
	value, found = mapping["F"].(*bool)
	assert.True(t, found, "Proper type: boolean")
	assert.Same(t, &config.F, mapping["F"])
	assert.Equal(t, false, *value)
}

func TestParseKeysUsingPrefix(t *testing.T) {
	config := struct {
		Name string
	}{
		"namae",
	}

	mapping := ParseKeys(&config, "T_", "conf")

	assert.NotNil(t, mapping["T_Name"])
	assert.Nil(t, mapping["Name"])
	assert.Equal(t, len(mapping), 1)

	config2 := struct {
		Count int `conf:"quantity"`
	}{
		100,
	}

	mapping = ParseKeys(&config2, "T2_", "conf")

	assert.NotNil(t, mapping["T2_quantity"])
	assert.Nil(t, mapping["quantity"])
	assert.Equal(t, len(mapping), 1)
}

// Test nested (i.e. nested structs)
func TestParseKeysComplex(t *testing.T) {

	type Child struct {
		Name string
	}

	type Parent struct {
		Name  string
		Child Child
	}

	type GrandParent struct {
		Name  string
		Child Parent
	}

	child := Child{Name: "Ali"}
	parent := Parent{Name: "Tom", Child: child}
	grandparent := GrandParent{Name: "Lida", Child: parent}

	mapping := ParseKeys(&grandparent, "", "conf")
	assert.NotNil(t, mapping["Name"])
	assert.NotNil(t, mapping["Child_Name"])
	assert.NotNil(t, mapping["Child_Child_Name"])

	mapping = ParseKeys(&parent, "", "conf")
	assert.NotNil(t, mapping["Name"])
	assert.NotNil(t, mapping["Child_Name"])
	assert.Nil(t, mapping["Child_Child_Name"])

	mapping = ParseKeys(&child, "", "conf")
	assert.NotNil(t, mapping["Name"])
	assert.Nil(t, mapping["Child_Name"])
	assert.Nil(t, mapping["Child_Child_Name"])
}

func TestParseKeysIgnore(t *testing.T) {
	config := struct {
		Ignored string `conf:"-"`
		Public  string
	}{
		"Ignored",
		"Public",
	}

	mapping := ParseKeys(&config, "", "conf")

	assert.Nil(t, mapping["Ignored"])
	assert.NotNil(t, mapping["Public"])
	assert.Equal(t, 1, len(mapping))
}
