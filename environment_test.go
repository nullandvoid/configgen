package configgen

import (
	"github.com/stretchr/testify/assert"
	"math"
	"os"
	"strconv"
	"testing"
)

func cleanup(conf interface{}) {
	mapping := ParseKeys(conf, "", "conf")
	for k, _ := range mapping {
		os.Unsetenv(k)
	}
}

func TestLoadEnvConfigDefaults(t *testing.T) {
	config := AllTypesConfig{
		String: "Alex",
		Int:    12,
		Bool:   true,
	}

	LoadEnvConfig(&config)

	assert.True(t, config.Bool)
	assert.Equal(t, 12, config.Int)
	assert.Equal(t, "Alex", config.String)

	cleanup(&config)
}

func TestLoadEnvConfigOverwrite(t *testing.T) {
	model := "Mclaren F1"
	age := "124"
	isNew := "false"

	os.Setenv("String", model)
	os.Setenv("Int", age)
	os.Setenv("Bool", isNew)

	config := AllTypesConfig{
		String: "CAR",
		Int:    1,
		Bool:   true,
	}

	LoadEnvConfig(&config)

	assert.False(t, config.Bool)
	assert.Equal(t, 124, config.Int)
	assert.Equal(t, model, config.String)
	cleanup(&config)
}

func TestLoadEnvConfigInts(t *testing.T) {
	config := AllTypesConfig{
		Int:    0,
		Int64:  0,
		Int32:  0,
		Int16:  0,
		Int8:   0,
		Uint:   0,
		Uint64: 0,
		Uint32: 0,
		Uint16: 0,
		Uint8:  0,
	}
	os.Setenv("Int", strconv.Itoa(math.MaxInt32))
	os.Setenv("Int64", strconv.FormatInt(math.MaxInt64, 10))
	os.Setenv("Int32", strconv.Itoa(math.MaxInt32))
	os.Setenv("Int16", strconv.Itoa(math.MaxInt16))
	os.Setenv("Int8", strconv.Itoa(math.MaxInt8))
	os.Setenv("Uint", strconv.FormatUint(math.MaxUint32, 10))
	os.Setenv("Uint64", strconv.FormatUint(math.MaxUint64, 10))
	os.Setenv("Uint32", strconv.FormatUint(math.MaxUint32, 10))
	os.Setenv("Uint16", strconv.FormatUint(math.MaxUint16, 10))
	os.Setenv("Uint8", strconv.FormatUint(math.MaxUint8, 10))
	LoadEnvConfig(&config)
	assert.Equal(t, int(math.MaxInt32), config.Int, "Int value mismatch")
	assert.Equal(t, int64(math.MaxInt64), config.Int64, "Int64 value mismatch")
	assert.Equal(t, int32(math.MaxInt32), config.Int32, "Int32 value mismatch")
	assert.Equal(t, int16(math.MaxInt16), config.Int16, "Int16 value mismatch")
	assert.Equal(t, int8(math.MaxInt8), config.Int8, "Int8 value mismatch")
	assert.Equal(t, uint(math.MaxUint32), config.Uint, "Uint value mismatch")
	assert.Equal(t, uint64(math.MaxUint64), config.Uint64, "Uint64 value mismatch")
	assert.Equal(t, uint32(math.MaxUint32), config.Uint32, "Uint32 value mismatch")
	assert.Equal(t, uint16(math.MaxUint16), config.Uint16, "Uint16 value mismatch")
	assert.Equal(t, uint8(math.MaxUint8), config.Uint8, "Uint8 value mismatch")

	cleanup(&config)
}

func TestLoadEnvConfigFloats(t *testing.T) {
	config := AllTypesConfig{
		Float32: 12,
		Float64: 12,
	}
	os.Setenv("Float64", strconv.FormatFloat(math.MaxFloat64, 'E', -1, 64))
	os.Setenv("Float32", strconv.FormatFloat(math.MaxFloat32, 'E', -1, 32))
	LoadEnvConfig(&config)
	assert.Equal(t, math.MaxFloat64, config.Float64)
	assert.Equal(t, float32(math.MaxFloat32), config.Float32)
	cleanup(&config)
}

func TestLoadEnvConfigNilPanic(t *testing.T) {
	assert.Panics(t, func() { LoadEnvConfig(nil) }, "Nil should cause a panic")
}

func TestLoadEnvConfigBoolPanic(t *testing.T) {
	config := AllTypesConfig{
		Bool: false,
	}
	assert.Panics(t, func() {
		os.Setenv("Bool", "12345")
		LoadEnvConfig(&config)
	}, "Bad OS Bool Env should cause panic")
	assert.False(t, config.Bool)
	cleanup(&config)
}

func TestLoadEnvConfigFloatPanic(t *testing.T) {
	config := AllTypesConfig{
		Float64: 0,
		Float32: 0,
	}
	assert.Panics(t, func() {
		os.Setenv("Float64", "Float64")
		os.Setenv("Float32", "0")
		LoadEnvConfig(&config)
	}, "Bad OS Float64 Env should cause panic")

	assert.Panics(t, func() {
		os.Setenv("Float64", "0")
		os.Setenv("Float32", "Float32")
		LoadEnvConfig(&config)
	}, "Bad OS Float32 Env should cause panic")

	assert.Equal(t, float64(0), config.Float64)
	assert.Equal(t, float32(0), config.Float32)
	cleanup(&config)
}

func TestLoadEnvConfigIntPanic(t *testing.T) {
	config := AllTypesConfig{
		Int: 0,
	}
	assert.Panics(t, func() {
		os.Setenv("Int", "Bobby")
		LoadEnvConfig(&config)
	}, "Bad OS Int Env should cause panic")
	assert.Equal(t, 0, config.Int)
	cleanup(&config)
}
