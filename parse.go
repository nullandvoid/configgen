package configgen

import (
	"reflect"
	"unsafe"
)

// Takes an annotated struct and returns a map containing the key and object
func ParseKeys(s interface{}, prefix string, tag string) map[string]interface{} {
	var v reflect.Value
	v = reflect.ValueOf(s).Elem()

	// Return the object type
	configType := v.Type()

	keyMap := make(map[string]interface{})

	// Go through all the keys of the struct
	for i := 0; i < configType.NumField(); i++ {
		fieldDefinition := configType.Field(i)
		field := v.FieldByName(fieldDefinition.Name)
		tagValue := fieldDefinition.Tag.Get(tag)
		if tagValue == "" {
			tagValue = fieldDefinition.Name
		} else if tagValue == "-" {
			// Allow dash as an explicit exclude
			continue
		}
		tagValue = prefix + tagValue
		var val interface{}
		// Figure out the field type and cast the interface to it
		switch field.Kind() {
		case reflect.Bool:
			val = (*bool)(unsafe.Pointer(field.Addr().Pointer()))

		case reflect.Int64:
			val = (*int64)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Int32:
			val = (*int32)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Int16:
			val = (*int16)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Int8:
			val = (*int8)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Int:
			val = (*int)(unsafe.Pointer(field.Addr().Pointer()))

		case reflect.Uint64:
			val = (*uint64)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Uint32:
			val = (*uint32)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Uint16:
			val = (*uint16)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Uint8:
			val = (*uint8)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Uint:
			val = (*uint)(unsafe.Pointer(field.Addr().Pointer()))

		case reflect.Float64:
			val = (*float64)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Float32:
			val = (*float32)(unsafe.Pointer(field.Addr().Pointer()))

		case reflect.String:
			val = (*string)(unsafe.Pointer(field.Addr().Pointer()))
		case reflect.Struct:
			if val == nil {
				val = field.Addr().Interface()
			}
			subMapping := ParseKeys(val, tagValue+"_", tag)
			for k, v := range subMapping {
				keyMap[k] = v
			}
			val = nil
		}
		if val != nil {
			keyMap[tagValue] = val
		}
	}

	return keyMap
}
